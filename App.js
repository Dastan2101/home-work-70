import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

export default class App extends React.Component {

    constructor() {
        super();

        this.operations = ['<', 'CE', '+', '-', '*', '/'];
    }

    state = {
        result: '',
        calculation: '',
    };

    calculateResult = () => {
        const resultText = this.state.result;

        let calculating;
        try {
            calculating = eval(resultText);
            this.setState({calculation: calculating})
        } catch (e) {

            if (e instanceof SyntaxError) {
                this.setState({calculation: e})
            }
        }

    };

    buttonPressed = numb => {

        if (this.state.result.indexOf('error') === -1) {
            if (numb === '=') {
                return this.validate() && this.calculateResult()
            }
            this.setState({result: this.state.result + numb});

            if (numb === '.') {
                if (this.state.result.indexOf(numb) !== -1) {
                    this.setState({result: 'error'})

                }
            }
        } else {
            this.setState({calculation: 'ERROR'})
        }


    };

    operate = (operation) => {
        switch (operation) {
            case '<':
                if (this.state.result !== 'error') {
                    let text = this.state.result.split('');
                    text.pop();
                    this.setState({result: text.join(''), calculation: ''});
                    break;
                } else {
                    this.setState({result: '', calculation: ''});
                    break;
                }

            case 'CE':
                this.setState({result: '', calculation: ''});
                break;
            case '+':
            case '-':
            case '*':
            case '/':

                const lastChar = this.state.result.split('').pop();

                if (this.operations.indexOf(lastChar) > 0) return;
                if (this.state.result === '') return;

                this.setState({result: this.state.result + operation});
                break;
        }
    };

    validate = () => {

        const validate = this.state.result;
        switch (validate.slice(-1)) {
            case '+':
            case '-':
            case '*':
            case '/':
            case '.':
                return false
        }
        return true
    };

    render() {

        let rows = [];
        let nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9], ['.', 0, '=']];

        for (let i = 0; i < nums.length; i++) {
            let row = [];
            for (let j = 0; j < 3; j++) {
                row.push(
                    <TouchableOpacity
                        key={nums[i][j]}
                        style={styles.btn}

                        onPress={() => this.buttonPressed(nums[i][j])}
                    >
                        <Text style={styles.btnText}>{nums[i][j]}
                        </Text>
                    </TouchableOpacity>
                )
            }
            rows.push(<View key={i} style={styles.row}>{row}</View>)
        }

        let ops = [];

        for (let i = 0; i < this.operations.length; i++) {
            ops.push(
                <TouchableOpacity
                    style={styles.btn}
                    key={this.operations[i]}
                    onPress={() => this.operate(this.operations[i])}
                >
                    <Text
                        style={styles.color}>{this.operations[i]}
                    </Text>
                </TouchableOpacity>
            )
        }

        return (
            <View style={styles.container}>
                <View style={styles.result}>
                    <Text style={styles.resultText}>{this.state.result}</Text>

                </View>
                <View style={styles.calculation}>
                    <Text style={styles.calculationText}>{this.state.calculation}</Text>

                </View>
                <View style={styles.buttons}>
                    <View style={styles.numbers}>
                        {rows}
                    </View>
                    <View style={styles.operations}>
                        {ops}
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    resultText: {
        fontSize: 35,
        color: 'black',
        paddingRight: 15
    },
    btnText: {
        fontSize: 40,
        color: 'white'
    },
    color: {
        color: 'black',
        fontSize: 40
    },
    calculationText: {
        fontSize: 35,
        color: '#8d8b8e',
        paddingRight: 15

    },
    btn: {
        flex: 1,
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'center',
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    result: {
        flex: 2,
        backgroundColor: '#fffeff',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    calculation: {
        flex: 1,
        backgroundColor: '#fffeff',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    buttons: {
        flex: 7,
        flexDirection: 'row',

    },
    numbers: {
        flex: 3,
        backgroundColor: '#454246',

    },
    operations: {
        flex: 1,
        backgroundColor: '#656265',
        justifyContent: 'space-around',
    }

});
